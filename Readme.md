<!-- omit in toc -->
# LightTransactions

**Warning:** This project is still a work-in-progress.  
It seems to work in a normal jar project. If it also works in a Servlet Container has yet to be proven.

A lightweight Transactions Framework for the Java/Jakarta Persistence API.  
It brings the `@LightTransactional` annotation which similarly to the `@Transactional` annotation of the Java/Jakarta Transaction API can be put on methods and types.

<!-- omit in toc -->
## Table of Contents

- [Usage](#usage)
  - [Setup](#setup)
  - [Annotation](#annotation)
  - [Configuration](#configuration)
- [Under the Hood](#under-the-hood)
  - [Inheritance](#inheritance)

## Usage

### Setup

For the transactions to be inserted at your applications startup, declare `LightTransactionListener` as a listener in your `web.xml`.  
There are two classes of that name. Choose the one corresponding to the enterprise namespace you use:

- `dev.jelemux.lighttransactions.javax.listener.LightTransactionListener`
- `dev.jelemux.lighttransactions.jakarta.listener.LightTransactionListener`

Then add it to your `web.xml` as follows:

```xml
<?xml version="1.0"?>
<web-app 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    xmlns="http://java.sun.com/xml/ns/javaee" 
    xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd" 
    version="4.0">

    <listener>
        <listener-class>dev.jelemux.lighttransactions.javax.listener.LightTransactionListener</listener-class>
    </listener>

</web-app>
```

If you can't or don't want to use the listener, you can also directly use the `LightTransactionInserter` class:

```java
LightTransactionInserter inserter = new LightTransactionInserter();
inserter.scanAndInsert();
```

### Annotation

```java
@Transactional(

    /* Name of the EntityManager variable
     * Default: entityManager
     */
    entityManagerName = "yourEntityManager",

    /* Initiate the rollback when this Exception is thrown
     * Default: java.lang.Exception.class
     */
    rollbackOn = YourException.class,

    /* Inherit this annotation to subtypes. In contrary to Java standard, this also works on methods.
     * Default: false
     */
    inherit = true
)
```

### Configuration

Configuration is done in a file `lighttransactions.properties` under `src/main/resources`.  

```properties
# The search path to look for your annotated items. If your project is very big, limiting this may increase startup performance.
# Default: "" (empty string)
search_path=your.package
# The enterprise namespace to use options are javax and jakarta
# Default: jakarta
enterprise_namespace=javax
```

## Under the Hood

This library is meant to insert the transactions at runtime, preferrably during startup of the application.  
To accomplish that it must first scan for annotations and then manipulate the Java byte code of the affected methods.

Scanning for annotations works through the org.reflections library,  
finding the affected methods is done via Java reflection  
and for byte code manipulation the Javassist library is used.

### Inheritance

Because `inherited` is just an annotation parameter the inheritance here cannot be a real inheritance as an annotation with the `@Inherited` annotation would have it.  
Instead, this is accomplished through Java reflection.
