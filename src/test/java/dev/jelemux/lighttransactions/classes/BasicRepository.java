/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.classes;

import dev.jelemux.lighttransactions.annotation.LightTransactional;
import jakarta.enterprise.inject.Vetoed;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import java.util.List;

/**
 * 
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 * @param <ENTITY> 
 */
@Vetoed
@LightTransactional(inherit = true)
public abstract class BasicRepository<ENTITY> {

    @Inject
    protected EntityManager entityManager;

    protected Class<ENTITY> entityClass;

    public BasicRepository(Class<ENTITY> entityKlasse) {
        this.entityClass = entityKlasse;
    }

    protected List<ENTITY> getAll() {
        CriteriaBuilder cb = entityManager.getCriteriaBuilder();
        CriteriaQuery<ENTITY> cq = cb.createQuery(entityClass);
        CriteriaQuery<ENTITY> all = cq.select(cq.from(entityClass));
        return entityManager.createQuery(all).getResultList();
    }

    @LightTransactional
    public ENTITY merge(ENTITY e) {
        ENTITY result = this.entityManager.merge(e);
        return result;
    }

    public Object findById(Long id) {
        return this.entityManager.find(this.entityClass, id);
    }

    @LightTransactional(inherit = true)
    protected void delete(Long id) {
        this.entityManager.remove(this.entityManager.getReference(this.entityClass, id));
    }
}
