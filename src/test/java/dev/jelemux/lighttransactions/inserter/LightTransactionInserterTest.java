/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.inserter;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.bytecode.ClassFile;
import static org.junit.jupiter.api.Assertions.assertTrue;
import org.junit.jupiter.api.Test;

/**
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class LightTransactionInserterTest {

    /**
     * Test of scanAndInsert method, of class LightTransactionListener.
     */
    @Test
    public void testScanAndInsert() throws Exception {
        System.out.println("scanAndInsert");
        
        LightTransactionInserter instance = new LightTransactionInserter();
        instance.scanAndInsert();
        
        ClassPool pool = ClassPool.getDefault();
        CtClass ctClass = pool.get("dev.jelemux.lighttransactions.classes.BasicRepository");
        ctClass.defrost();
        ClassFile cf = ctClass.getClassFile();
        
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        cf.write(new DataOutputStream(baos));
        
        String byteCode = baos.toString();
        
        assertTrue(byteCode.contains("rollback"));
        assertTrue(byteCode.contains("commit"));
        assertTrue(byteCode.contains("begin"));
    }
    
}
