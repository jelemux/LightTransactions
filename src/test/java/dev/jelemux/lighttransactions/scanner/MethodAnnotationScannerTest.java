/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.scanner;

import dev.jelemux.lighttransactions.classes.BasicRepository;
import dev.jelemux.lighttransactions.config.AnnotationConfig;
import dev.jelemux.lighttransactions.config.GlobalConfig;
import dev.jelemux.lighttransactions.error.AnnotationNotFoundException;
import dev.jelemux.lighttransactions.model.TransactionalMethod;
import java.lang.reflect.Method;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;

/**
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class MethodAnnotationScannerTest {
    
    private final MethodAnnotationScanner methodAnnotationScanner;
    
    public MethodAnnotationScannerTest() {
        GlobalConfig config = new GlobalConfig();
        Reflections reflections = config.configureReflections();
        
        methodAnnotationScanner = new MethodAnnotationScanner(reflections);
    }
    
    @Test
    public void testGetAnnotatedItems() {
        System.out.println("getAnnotatedItems");
        
        Set<TransactionalMethod> transactionalMethods = methodAnnotationScanner.getAnnotatedItems();
        assertEquals(4, transactionalMethods.size());
    }

    /**
     * Test of getDirectlyAnnotatedItems method, of class MethodAnnotationScanner.
     */
    @Test
    public void testGetDirectlyAnnotatedItems() {
        System.out.println("getDirectlyAnnotatedItems");
        
        Set<Method> directlyAnnotated = methodAnnotationScanner.getDirectlyAnnotatedItems();
        assertEquals(3, directlyAnnotated.size());
    }

    /**
     * Test of getInheritanceItems method, of class MethodAnnotationScanner.
     */
    @Test
    public void testGetInheritanceItems() throws NoSuchMethodException, AnnotationNotFoundException {
        System.out.println("getInheritanceItems");
        
        Method parentMethod = BasicRepository.class.getDeclaredMethod("delete", Long.class);
        Set<TransactionalMethod> overridingMethods = methodAnnotationScanner.getInheritanceItems(new TransactionalMethod(parentMethod, new AnnotationConfig(parentMethod), false));
        assertEquals(1, overridingMethods.size());
    }
    
}
