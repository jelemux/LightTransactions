/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.scanner;

import dev.jelemux.lighttransactions.config.GlobalConfig;
import dev.jelemux.lighttransactions.model.TransactionalMethod;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;

/**
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class TransactionalMethodAccumulatorTest {
    
    private final TransactionalMethodAccumulator tma;
    
    public TransactionalMethodAccumulatorTest() {
        GlobalConfig config = new GlobalConfig();
        Reflections reflections = config.configureReflections();
        
        tma = new TransactionalMethodAccumulator(reflections);
    }

    /**
     * Test of getMethods method, of class TransactionalMethodAccumulator.
     */
    @Test
    public void testGetMethods() {
        System.out.println("getMethods");
        
        Set<TransactionalMethod> transactionalMethods = tma.getMethods();
        assertEquals(6, transactionalMethods.size());
    }
    
}
