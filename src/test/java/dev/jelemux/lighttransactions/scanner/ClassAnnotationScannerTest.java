/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.scanner;

import dev.jelemux.lighttransactions.classes.BasicRepository;
import dev.jelemux.lighttransactions.config.AnnotationConfig;
import dev.jelemux.lighttransactions.config.GlobalConfig;
import dev.jelemux.lighttransactions.error.AnnotationNotFoundException;
import dev.jelemux.lighttransactions.model.TransactionalClass;
import java.util.Set;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;

/**
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class ClassAnnotationScannerTest {
    
    private final ClassAnnotationScanner classAnnotationScanner;
    
    public ClassAnnotationScannerTest() {
        GlobalConfig config = new GlobalConfig();
        Reflections reflections = config.configureReflections();
        
        classAnnotationScanner = new ClassAnnotationScanner(reflections);
    }
    
    @Test
    public void testGetAnnotatedItems() {
        System.out.println("getAnnotatedItems");
        
        Set<TransactionalClass> transactionalClasses = classAnnotationScanner.getAnnotatedItems();
        assertEquals(2, transactionalClasses.size());
    }

    /**
     * Test of getDirectlyAnnotatedItems method, of class ClassAnnotationScanner.
     */
    @Test
    public void testGetDirectlyAnnotatedItems() {
        System.out.println("getDirectlyAnnotatedItems");
        
        Set<Class<?>> directlyAnnotated = classAnnotationScanner.getDirectlyAnnotatedItems();
        assertEquals(1, directlyAnnotated.size());
    }

    /**
     * Test of getInheritanceItems method, of class ClassAnnotationScanner.
     * @throws dev.jelemux.lighttransactions.error.AnnotationNotFoundException
     */
    @Test
    public void testGetInheritanceItems() throws AnnotationNotFoundException {
        System.out.println("getInheritanceItems");
        
        Class<?> clazz = BasicRepository.class;
        TransactionalClass annotatedClass = new TransactionalClass(clazz, new AnnotationConfig(clazz), false);
        
        Set<TransactionalClass> subClasses = classAnnotationScanner.getInheritanceItems(annotatedClass);
        assertEquals(1, subClasses.size());
    }
    
}
