/*
 * The MIT License
 *
 * Copyright 2021 Jeremias Weber.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dev.jelemux.lighttransactions.javax.listener;

import java.io.File;

import org.apache.catalina.LifecycleState;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.impl.base.exporter.zip.ZipExporterImpl;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class LightTransactionListenerTest {

    private static final String WEBAPP_SRC = "src/test/webapp";
    private static final String APPLICATION_ID = "test_app";

    private Tomcat mTomcat;
    private String mWorkingDir = System.getProperty("java.io.tmpdir");

    @BeforeAll
    public void setup() throws Throwable {
        mTomcat = new Tomcat();
        mTomcat.setPort(0);
        mTomcat.setBaseDir(mWorkingDir);
        mTomcat.getHost().setAppBase(mWorkingDir);
        mTomcat.getHost().setAutoDeploy(true);
        mTomcat.getHost().setDeployOnStartup(true);
        
        String contextPath = "/" + APPLICATION_ID;
        File webApp = new File(mWorkingDir, APPLICATION_ID);
        File oldWebApp = new File(webApp.getAbsolutePath());
        FileUtils.deleteDirectory(oldWebApp);
        new ZipExporterImpl(createWebArchive()).exportTo(new File(mWorkingDir + "/" + APPLICATION_ID + ".war"),
                true);
        mTomcat.addWebapp(mTomcat.getHost(), contextPath, webApp.getAbsolutePath());

        mTomcat.start();
    }

    @AfterAll
    public final void teardown() throws Throwable {
        if (mTomcat.getServer() != null
                && mTomcat.getServer().getState() != LifecycleState.DESTROYED) {
            if (mTomcat.getServer().getState() != LifecycleState.STOPPED) {
                    mTomcat.stop();
            }
            mTomcat.destroy();
        }
    }


    @Test
    public void test() throws Exception {
    }

    private WebArchive createWebArchive() {
        WebArchive archive = ShrinkWrap.create(WebArchive.class, APPLICATION_ID + ".war");

        return archive
        .setWebXML(new File(WEBAPP_SRC, "WEB-INF/web.xml"))
        .addPackage(
                this.getClass().getClassLoader()
                .getDefinedPackage("dev.jelemux.lighttransactions")
        )
        .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
    }

    protected int getTomcatPort() {
        return mTomcat.getConnector().getLocalPort();
    }    
    
}
