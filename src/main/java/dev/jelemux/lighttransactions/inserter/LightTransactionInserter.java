/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.inserter;

import dev.jelemux.lighttransactions.config.GlobalConfig;
import dev.jelemux.lighttransactions.error.InvalidConfigurationException;
import dev.jelemux.lighttransactions.error.InvalidTransactionException;
import dev.jelemux.lighttransactions.model.TransactionalMethod;
import dev.jelemux.lighttransactions.scanner.TransactionalMethodAccumulator;
import java.io.IOException;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javassist.CannotCompileException;
import javassist.NotFoundException;
import org.reflections.Reflections;

/**
 * Manager that executes all the actions for finding the @LightTransactional annotation,
 * then inserting the Transactions into the Java bytecode 
 * and writing the modified classes into the classpath.
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class LightTransactionInserter {
    
    private final TransactionalMethodAccumulator accumulator;
    
    private final GlobalConfig globalConfig;
    
    public LightTransactionInserter() throws InvalidConfigurationException {
        globalConfig = new GlobalConfig();
        Reflections reflections = globalConfig.configureReflections();
        
        accumulator = new TransactionalMethodAccumulator(reflections);
    }
    
    /**
     * Method to scan the classpath (or rather the specified path in the config file) for the @LightTransactional annotation
     * and then insert the transactions into the Java byte code.
     */
    public void scanAndInsert() throws InvalidTransactionException {
        MethodTransactionInserter inserter = new MethodTransactionInserter(globalConfig);
        
        Set<TransactionalMethod> transactionalMethods = accumulator.getMethods();
        for (TransactionalMethod transactionalMethod : transactionalMethods) {
            try {
                inserter.insertTransaction(transactionalMethod);
            } catch (CannotCompileException | NotFoundException ex) {
                throw new InvalidTransactionException("Could not insert transaction into method " 
                        + transactionalMethod.getValue().getName() 
                        + " of class " + transactionalMethod.getValue().getDeclaringClass().getCanonicalName(), 
                        ex
                );
            }
        }
        
        try {
            inserter.write();
        } catch (IOException | NotFoundException | CannotCompileException ex) {
            Logger.getLogger(LightTransactionInserter.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
