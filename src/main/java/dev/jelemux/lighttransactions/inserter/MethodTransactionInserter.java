/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.inserter;

import dev.jelemux.lighttransactions.config.AnnotationConfig;
import dev.jelemux.lighttransactions.config.GlobalConfig;
import dev.jelemux.lighttransactions.model.TransactionalMethod;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;

/**
 * Inserter for inserting transactions into methods by manipulating their byte code.
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
class MethodTransactionInserter {
    
    private static final ClassPool pool = ClassPool.getDefault();
    
    private final Set<CtClass> classes = new HashSet<>();
    
    private final GlobalConfig globalConfig;
    
    public MethodTransactionInserter(GlobalConfig globalConfig) {
        this.globalConfig = globalConfig;
    }
    
    /**
     * Inserts a transaction into the method.
     * 
     * This does not write the modified method to the classpath.
     * To write to the classpath, call write().
     */
    public void insertTransaction(TransactionalMethod method) 
            throws NotFoundException, CannotCompileException {
        
        AnnotationConfig config = method.getConfig();
        CtMethod ctMethod = fromMethod(method);
        
        ctMethod.insertBefore(
                this.beginTransaction(method)
        );
        ctMethod.insertAfter(
                this.commitTransaction(method)
        );
        
        CtClass exceptionClass = pool.get(
                method.getConfig()
                        .getCanonicalExceptionName()
        );
        ctMethod.addCatch(
                this.rollbackTransaction(method), 
                exceptionClass
        );
        
        classes.add(ctMethod.getDeclaringClass());
    }
    
    /**
     * Write all classes modified by this inserter to the classpath.
     */
    public void write() 
            throws IOException, NotFoundException, CannotCompileException {
        for (CtClass clazz : classes) {
            clazz.writeFile();
        }
    }
    
    private CtMethod fromMethod(TransactionalMethod method) 
            throws NotFoundException {
        String className = method.getValue()
                .getDeclaringClass().getCanonicalName();
        String methodName = method.getValue()
                .getName();
        
        CtMethod ctMethod = pool.get(className)
                .getDeclaredMethod(methodName);
        return ctMethod;
    }
    
    private String beginTransaction(TransactionalMethod method) {
        return transactionManagerCallBuilder(method, "beginTransaction")
                .append("}").toString();
    }
    
    private String commitTransaction(TransactionalMethod method) {
        return transactionManagerCallBuilder(method, "commitTransaction")
                .append("}").toString();
    }
    
    private String rollbackTransaction(TransactionalMethod method) {
        return transactionManagerCallBuilder(method, "rollbackTransaction")
                .append("throw $e;\n}").toString();
    }
    
    private StringBuilder transactionManagerCallBuilder(TransactionalMethod method, String transactionMethodCall) {
        return new StringBuilder().append("{\n")
                .append("dev.jelemux.lighttransactions.")
                .append(globalConfig.getEnterpriseNamespace().toString())
                .append(".manager.LightTransactionManager")
                .append("#getInstance().").append(transactionMethodCall).append("($0.").append(method.getConfig().getEntityManagerName())
                .append(", dev.jelemux.lighttransactions.util.TransactionUtil#methodFromClass($0.getClass(), \"").append(method.getValue().getName()).append("\", \"")
                .append(arrayOfClassesToString(method.getValue().getParameterTypes())).append("\"));\n");
    }

    private StringBuilder arrayOfClassesToString(Class<?>[] parameterTypes) {
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < parameterTypes.length; i++) {
            builder.append(parameterTypes[i]);
            if (i != parameterTypes.length - 1) {
                builder.append(",");
            }
        }
        return builder;
    }
    
}
