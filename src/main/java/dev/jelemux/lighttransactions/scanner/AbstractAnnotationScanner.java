/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.scanner;

import dev.jelemux.lighttransactions.config.AnnotationConfig;
import dev.jelemux.lighttransactions.error.AnnotationNotFoundException;
import dev.jelemux.lighttransactions.model.AbstractTransactionalItem;
import java.lang.reflect.AnnotatedElement;
import java.util.HashSet;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import org.reflections.Reflections;

/**
 * Abstract Scanner for Items with the @LightTransactional annotation.
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
@RequiredArgsConstructor
abstract class AbstractAnnotationScanner<T extends AnnotatedElement, I extends AbstractTransactionalItem<T>> {
    
    protected final Reflections reflections;
    
    protected abstract Set<T> getDirectlyAnnotatedItems();
    
    protected abstract I createAnnotatedItem(T value, AnnotationConfig config, boolean inherited);
    
    protected abstract Set<I> getInheritanceItems(I directlyAnnotatedItem);
    
    /**
     * Returns all Items of a specific type that have the @LightTransactional Annotation.
     */
    Set<I> getAnnotatedItems() {
        Set<T> directlyAnnotatedItems = this.getDirectlyAnnotatedItems();
        
        Set<I> allAnnotatedItems = new HashSet<>(directlyAnnotatedItems.size());
        for (T directlyAnnotatedItem : directlyAnnotatedItems) {
            try {
                AnnotationConfig config = new AnnotationConfig(directlyAnnotatedItem);
                I directlyAnnotatedItemWrapper = this.createAnnotatedItem(directlyAnnotatedItem, config, false);
                allAnnotatedItems.add(directlyAnnotatedItemWrapper);
                
                // Add inheritance items if the annotation is marked as inherited
                if (config.isInherit()) {
                    Set<I> inheritanceItems = this.getInheritanceItems(directlyAnnotatedItemWrapper);
                    allAnnotatedItems.addAll(inheritanceItems);
                }
            } catch (AnnotationNotFoundException ex) { }
        }
        return allAnnotatedItems;
    }
    
    @SuppressWarnings("unchecked")
    protected Set<Class<?>> getSubTypesOf(Class<?> type) {
        return reflections.getSubTypesOf((Class<Object>) type);
    }
    
}
