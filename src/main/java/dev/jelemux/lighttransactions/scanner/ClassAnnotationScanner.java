/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.scanner;

import dev.jelemux.lighttransactions.config.AnnotationConfig;
import dev.jelemux.lighttransactions.config.GlobalConfig;
import dev.jelemux.lighttransactions.model.TransactionalClass;
import java.util.Set;
import java.util.stream.Collectors;
import org.reflections.Reflections;

/**
 * Scanner for classes with the @LightTransactional annotation.
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
class ClassAnnotationScanner extends AbstractAnnotationScanner<Class<?>, TransactionalClass> {

    ClassAnnotationScanner(Reflections reflections) {
        super(reflections);
    }

    @Override
    protected Set<Class<?>> getDirectlyAnnotatedItems() {
        return reflections.getTypesAnnotatedWith(GlobalConfig.annotationClass, true);
    }

    @Override
    protected TransactionalClass createAnnotatedItem(Class<?> value, AnnotationConfig config, boolean inherited) {
        TransactionalClass annotatedClass = new TransactionalClass(value, config, inherited);
        return annotatedClass;
    }
    
    @Override
    protected Set<TransactionalClass> getInheritanceItems(TransactionalClass annotatedClass) {
        @SuppressWarnings("unchecked")
        Set<Class<? extends Object>> subTypes = this.getSubTypesOf(annotatedClass.getValue());
        
        return subTypes.stream()
                .map(t -> 
                        new TransactionalClass(
                                t, 
                                annotatedClass.getConfig(), 
                                true
                        )
                ).collect(Collectors.toSet());
    }
    
}
