/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.scanner;

import dev.jelemux.lighttransactions.model.TransactionalClass;
import dev.jelemux.lighttransactions.model.TransactionalMethod;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.reflections.Reflections;

/**
 * Accumulator for all methods that are affected by @LightTransactional annotation.
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class TransactionalMethodAccumulator {
    
    private final ClassAnnotationScanner classAnnotationScanner;
    
    private final MethodAnnotationScanner methodAnnotationScanner;
    
    public TransactionalMethodAccumulator(Reflections reflections) {
        this.classAnnotationScanner = new ClassAnnotationScanner(reflections);
        this.methodAnnotationScanner = new MethodAnnotationScanner(reflections);
    }
    
    /**
     * Returns all methods affected by @LightTransactional annotations.
     */
    public Set<TransactionalMethod> getMethods() {
        
        Set<TransactionalMethod> transactionalMethods = this.methodAnnotationScanner.getAnnotatedItems();
        for (TransactionalClass transactionalClass : this.classAnnotationScanner.getAnnotatedItems()) {
            transactionalMethods.addAll(
                    this.getAllMethodsFromClass(transactionalClass)
            );
        }
        
        return removeDuplicates(transactionalMethods);
    }
    
    private Set<TransactionalMethod> getAllMethodsFromClass(TransactionalClass transactionalClass) {
        return Arrays.stream(
                transactionalClass.getValue()
                        .getDeclaredMethods()
        ).map(
                m -> new TransactionalMethod(
                        m, 
                        transactionalClass.getConfig(), 
                        transactionalClass.isInherited(),
                        true
                )
        ).collect(Collectors.toSet());
    }
    
    private Set<TransactionalMethod> removeDuplicates(Set<TransactionalMethod> transactionalMethods) {
        Set<Method> uniqueMethods = new HashSet<>(transactionalMethods.size());
        return transactionalMethods.stream().filter(
                m -> {
                    boolean include = !uniqueMethods.contains(m.getValue());
                    uniqueMethods.add(m.getValue());
                    return include;
                }
        ).collect(Collectors.toSet());
    }
    
}
