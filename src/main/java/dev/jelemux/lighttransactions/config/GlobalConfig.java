/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.config;

import dev.jelemux.lighttransactions.annotation.LightTransactional;
import dev.jelemux.lighttransactions.error.InvalidConfigurationException;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import lombok.Getter;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;

/**
 * Global Configuration for this library.
 * This is read out of a lighttransactions.properties file under src/main/resources
 * 
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
@Getter
public class GlobalConfig {
    
    public static final Class<LightTransactional> annotationClass = LightTransactional.class;
    
    private final String searchPath;
    
    private final EnterpriseNamespace enterpriseNamespace;
    
    public GlobalConfig() throws InvalidConfigurationException {
        String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
        String configPath = rootPath + "lighttransactions.properties";
        
        Properties props = new Properties();
        try {
            props.load(new FileInputStream(configPath));
        } catch (IOException ex) {
            Logger.getLogger(GlobalConfig.class.getName()).log(
                    Level.INFO, 
                    "Could not find lighttransactions.properties in resources folder, using default values."
            );
        }
        
        this.searchPath = props.getProperty("search_path", "");
            
        String namespaceRaw = props.getProperty("enterprise_namespace");
        if (namespaceRaw == null) {
            this.enterpriseNamespace = EnterpriseNamespace.JAKARTA;
        } else switch (namespaceRaw.toLowerCase()) {
            case "jakarta":
                this.enterpriseNamespace = EnterpriseNamespace.JAKARTA;
                break;
            case "javax":
                this.enterpriseNamespace = EnterpriseNamespace.JAVAX;
                break;
            default:
                throw new InvalidConfigurationException(namespaceRaw + " is not a valid value for key 'enterprise_namespace', valid keys are 'javax', 'jakarta'");
        }
    }
    
    /**
     * @return Reflections instance with this configuration
     */
    public Reflections configureReflections() {
        ConfigurationBuilder configBuilder = 
                new ConfigurationBuilder().setScanners(
                        new TypeAnnotationsScanner(),
                        new MethodAnnotationsScanner(),
                        new SubTypesScanner()
                );
        
        if (searchPath != null) {
            configBuilder.setUrls(ClasspathHelper.forPackage(searchPath));
        }
        
        return new Reflections(configBuilder);
    }
    
}
