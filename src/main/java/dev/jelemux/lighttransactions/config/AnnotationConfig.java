/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.config;

import dev.jelemux.lighttransactions.annotation.LightTransactional;
import dev.jelemux.lighttransactions.error.AnnotationNotFoundException;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import lombok.Getter;

/**
 * Configuration for the @LightTransactional annotation
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
@Getter
public class AnnotationConfig {
    
    private final String entityManagerName;
    
    private final boolean inherit;
    
    private final Class<? extends Exception> rollbackOn;
    
    public <T extends AnnotatedElement> AnnotationConfig(T annotatedItem) throws AnnotationNotFoundException {
        this(annotatedItem.getAnnotations());
    }
    
    private AnnotationConfig(Annotation[] annotations) throws AnnotationNotFoundException {
        for(Annotation annotation : annotations){
            if(annotation instanceof LightTransactional){
                LightTransactional lighttransactional = (LightTransactional) annotation;
                
                this.entityManagerName = lighttransactional.entityManagerName();
                this.inherit = lighttransactional.inherit();
                this.rollbackOn = lighttransactional.rollbackOn();
                return;
            }
        }
        
        throw new AnnotationNotFoundException();
    }
    
    public String getCanonicalExceptionName() {
        return this.rollbackOn.getCanonicalName();
    }
    
}
