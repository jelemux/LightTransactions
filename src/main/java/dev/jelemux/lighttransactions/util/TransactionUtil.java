/*
 * The MIT License
 *
 * Copyright 2021 Jeremias Weber.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dev.jelemux.lighttransactions.util;

import dev.jelemux.lighttransactions.error.TransactionMethodNotFoundException;
import java.lang.reflect.Method;
import java.util.Arrays;

/**
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class TransactionUtil {
    
    public static Method methodFromClass(Class<?> clazz, String name, String paramerTypesAsString) {
        try {
            return clazz.getDeclaredMethod(
                    name,
                    parameterTypesFromStringArray(paramerTypesAsString)
            );
        } catch (NoSuchMethodException | SecurityException ex) {
            throw new TransactionMethodNotFoundException("Method " + clazz.getCanonicalName() + "#" + name 
                    + " with parameter types " + paramerTypesAsString + " could not be found.", ex);
        }
    }
    
    private static Class<?>[] parameterTypesFromStringArray(String paramerTypesAsString) {
        return Arrays.stream(paramerTypesAsString.split(",")).map(p -> {
            try {
                return Class.forName(p);
            } catch (ClassNotFoundException ex) {
                throw new TransactionMethodNotFoundException("ParameterType " + p + " could not be found.", ex);
            }
        }).toArray(size -> new Class<?>[size]);
    }
    
}
