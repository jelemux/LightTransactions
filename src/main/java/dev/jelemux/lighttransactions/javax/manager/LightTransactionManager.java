/*
 * The MIT License
 *
 * Copyright 2021 Jeremias Weber.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package dev.jelemux.lighttransactions.javax.manager;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.EntityManager;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * TransactionManager for the javax namespace.
 * Handles transactions in such a way so that only the outermost transactional method 
 * in a series of nested method calls begins, commits or rolls back transactions.
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LightTransactionManager {
    
    private static LightTransactionManager instance;
    
    private final Set<Method> ongoingTransactions = new HashSet<>();
    
    public static LightTransactionManager getInstance() {
        if (instance == null) {
            instance = new LightTransactionManager();
        }
        return instance;
    }
    
    public void beginTransaction(EntityManager entityManager, Method method) {
        if (!entityManager.getTransaction().isActive()) {
            entityManager.getTransaction().begin();
            this.ongoingTransactions.add(method);
        }
    }
    
    public void commitTransaction(EntityManager entityManager, Method method) {
        if (this.ongoingTransactions.contains(method)) {
            entityManager.getTransaction().commit();
            this.ongoingTransactions.remove(method);
        }
    }
    
    public void rollbackTransaction(EntityManager entityManager, Method method) {
        if (this.ongoingTransactions.contains(method)) {
            entityManager.getTransaction().rollback();
            this.ongoingTransactions.remove(method);
        }
    }
    
}
