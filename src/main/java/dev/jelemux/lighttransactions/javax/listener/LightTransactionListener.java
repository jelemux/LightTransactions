/*
 * Copyright (c) 2021 Jeremias Weber <jeremias.weber@protonmail.com>
 *
 * Permission is hereby granted, free of charge, to any
 * person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the
 * Software without restriction, including without
 * limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software
 * is furnished to do so, subject to the following
 * conditions:
 * 
 * The above copyright notice and this permission notice
 * shall be included in all copies or substantial portions
 * of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF
 * ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 * TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT
 * SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
 * IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 * DEALINGS IN THE SOFTWARE.
 * 
 */
package dev.jelemux.lighttransactions.javax.listener;

import dev.jelemux.lighttransactions.error.InvalidConfigurationException;
import dev.jelemux.lighttransactions.error.InvalidTransactionException;
import dev.jelemux.lighttransactions.inserter.LightTransactionInserter;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

/**
 * Listener for the javax namespace to insert the transactions at the start of the application.
 *
 * @author Jeremias Weber <jeremias.weber@protonmail.com>
 */
public class LightTransactionListener implements ServletContextListener {
    
    @Override
    public void contextInitialized(ServletContextEvent sce) 
            throws InvalidTransactionException, InvalidConfigurationException  {
        LightTransactionInserter manager = new LightTransactionInserter();
        manager.scanAndInsert();
    }
    
}
